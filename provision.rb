require('json')
require('net/ssh')
require('typhoeus')

# Check if correct number of arguments were specified.
if ARGV.length != 1
  puts "Usage: ruby provision.rb <region>"
  puts "Use quotes if the region name has a space."
  exit
end

# Read secrets.json
begin
  secrets = JSON.parse(File.read("secrets.json"))
rescue JSON::ParserError
  puts "Unable to parse secrets.json. Exiting."
  exit
end

# Get datacenter ID of specified region.
regions = Typhoeus.get("https://api.vultr.com/v1/regions/list").body
begin
  regions = JSON.parse(regions)
rescue JSON::ParserError
  puts "An error occurred when looking up the datacenter ID."
  puts "Reponse code: #{regions.code}"
  puts regions.body
  exit
end
dcid = ""
regions.each do |id, r|
  if r["name"].casecmp(ARGV[0]) == 0
    dcid = r["DCID"]
  end
end

# Determine TF2 region ID of specified region.
if regions[dcid]["continent"] == "North America"
  case ARGV[0]
    when "New Jersey", "Chicago", "Dallas", "Atlanta", "Toronto", "Miami"
      sv_region = 0
    when "Seattle", "Los Angeles", "Silicon Valley"
      sv_region = 1
  end
else
  region_array = [
    "NA East", # Unused
    "NA West", # Unused
    "South America",
    "Europe",
    "Asia",
    "Australia",
    "Middle East",
    "Africa"
  ]
  sv_region = region_array.find_index(regions[dcid]["continent"])
end

# Exit if specified region was not found.
if dcid == ""
  puts "Specified region not found. Exiting."
  exit
end

servercreate = Typhoeus.post(
  "https://api.vultr.com/v1/server/create",
  headers: {"API-Key" => secrets["api"]},
  body: {
    "DCID" => dcid,
    "VPSPLANID" => "203",	# Plan ID 203 is the $0.03/h plan.
    "OSID" => "362", 		# OS ID 362 is CentOS 8.
    "SSHKEYID" => secrets["ssh"],
    "hostname" => "tf2server"	# Hostname.
  }
)
begin
  subid = JSON.parse(servercreate.body)["SUBID"]
  puts "Server ID: #{subid}"
rescue JSON::ParserError
  puts "An error occurred when creating the server."
  puts "Response code: #{servercreate.code}"
  puts servercreate.body
  exit
end

# Check the server's status and power_status every 10 seconds.
serverlist = {}
status = "pending"
power_status = "stopped"
while (status == "pending" or power_status == "stopped") do
  sleep(10)
  begin
    serverlist = JSON.parse(Typhoeus.get(
      "https://api.vultr.com/v1/server/list",
      headers: {"API-Key" => secrets["api"]}
    ).body)
  rescue JSON::ParserError
    puts "An error occured while waiting for the server to provision."
    puts "Response code: #{serverlist.code}"
    puts serverlist.body
  end
  status = serverlist[subid]["status"]
  power_status = serverlist[subid]["power_status"]
end

# Determine server's IP address and password.
password = serverlist[subid]["default_password"]
ip = serverlist[subid]["main_ip"]
puts "Server IP: #{ip}"

# Attempt an SSH connection every 10 seconds.
ssh_reachable = false
while ssh_reachable == false
  sleep(10)
  begin
    Net::SSH.start(
      ip,
      "root",
      password: password,
      verify_host_key: false
    ) do |ssh|
      ssh.exec "hostname"
    end
    ssh_reachable = true
  rescue Errno::EHOSTUNREACH
    ssh_reachable = false
  end
end

# Generate random password for RCON.
rcon_password = ('a'..'z').to_a.shuffle[0,8].join

# Run Ansible playbook.
system("""ansible-playbook tf2.yml -i #{ip}, --extra-vars 'sv_region=#{sv_region} rcon_password=#{rcon_password}'""")
