#!/bin/sh
if ! screen -list | grep -q "tf2"
 then
  screen -d -m -S tf2 tf2/srcds_run -game tf $(cat launchoptions.txt)
  pgrep -f srcds_run > /home/tf/tf2server.pid
 else
  echo "TF2 server is already running! Use stop.sh to stop OR \`systemctl stop tf2.service\` if using systemd."
 exit 1
fi
